from django.contrib.auth import authenticate, login, logout
from django.core.exceptions import PermissionDenied
from django.core.handlers.wsgi import WSGIRequest
from django.shortcuts import redirect
from django.views import View
from django.views.generic import TemplateView

from app_auth.forms import LoginForm


class LoginView(TemplateView):
    template_name = "login.html"

    def get(self, request: WSGIRequest, *args, **kwargs):
        context = self.get_context_data(form=LoginForm())
        return self.render_to_response(context)

    def post(self, request: WSGIRequest, *args, **kwargs):
        data = LoginForm(request.POST)
        if data.is_valid():
            user = authenticate(request,
                                username=data.cleaned_data["login"],
                                password=data.cleaned_data["password"], )
            if user:
                login(request, user)
                return redirect('/polls/')
        raise PermissionDenied()


class LogoutView(View):
    def get(self, request: WSGIRequest, *args, **kwargs):
        logout(request)
        return redirect("/auth/login")
# Create your views here.
