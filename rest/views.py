from django.shortcuts import render
from rest_framework import viewsets, permissions
from rest_framework.relations import PrimaryKeyRelatedField

from polls.models import Poll, Choice
from rest.serializer import PollSerializer, ChoiceSerializer


class PollViewSet(viewsets.ModelViewSet):
    queryset = Poll.objects.order_by("-pub_date").prefetch_related("choice_set").all()
    serializer_class = PollSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    choices = PrimaryKeyRelatedField(pk_field="choice_set", many=True, read_only=True)


class ChoiceViewSet(viewsets.ModelViewSet):
    queryset = Choice.objects.all()
    serializer_class = ChoiceSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
# Create your views here.
