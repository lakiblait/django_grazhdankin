from rest_framework import serializers
from rest_framework.relations import PrimaryKeyRelatedField

from polls.models import Poll, Choice


class ChoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Choice
        fields = ["poll", "choice_text", "votes"]
        read_only_fields = ("votes",)


class PollSerializer(serializers.ModelSerializer):
    class Meta:
        model = Poll
        fields = ["questions", "pub_date", "choice_set"]

    # choice_set = ChoiceSerializer(many=True, read_only=True)
    choice_set = PrimaryKeyRelatedField(many=True, read_only=True)
