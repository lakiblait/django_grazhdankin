from rest_framework import routers
from rest import views

router = routers.DefaultRouter()
router.register("poll", views.PollViewSet)
router.register("choice", views.ChoiceViewSet)
urlpatterns = router.urls
