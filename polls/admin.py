from django.contrib import admin
from polls.models import Poll, Choice


def remove_choices(modeladmin, request, queryset):
    Choice.objects.filter(poll_in=queryset).delete()


remove_choices.short_description = "Remove all choices"


class ChoiceInline(admin.StackedInline):
    model = Choice
    extra = 1


@admin.register(Poll)
class PollAdmin(admin.ModelAdmin):
    inlines = [ChoiceInline]
    actions = [remove_choices]
