from django.urls import path

from polls.views import PollsView, ChoiceView


urlpatterns = [
    # path("<int:poll_id>/choice/<int:choice_id>", ChoiceView.as_view()),
    path("", PollsView.as_view()),
]
