from django.contrib.auth import login, authenticate
from django.core.exceptions import PermissionDenied
from django.core.paginator import Paginator
from django.http import HttpResponse
from django.shortcuts import render
from django.core.handlers.wsgi import WSGIRequest
from django.shortcuts import redirect
from django.views import View
from django.views.generic import TemplateView


from polls.models import Poll, Choice


class PollsView(TemplateView):
    template_name = "polls.html"

    def get(self, request: WSGIRequest, *args, **kwargs):
        objects = Poll.objects.prefetch_related("choice.set").all()
        p = Paginator(objects, 10)
        page = request.GET.get('page', 1)

        context = self.get_context_data(page_obj=p.get_page(page))

        # context = self.get_context_data(polls=Poll.objects.all())
        return self.render_to_response(context)


class ChoiceView(View):
    def get(self, request: WSGIRequest, poll_id, choice_id, *args, **kwargs):
        choice = Choice.objects.get(id=choice_id, poll_id=poll_id)
        choice.votes += 1
        choice.save()
        return redirect(f"/polls/")


# Create your views here.

