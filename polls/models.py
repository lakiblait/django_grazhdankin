from django.db import models


class Poll(models.Model):
    # class Meta:
    #     unique_together = ['album', 'order']
    #     ordering = ['-pub_date']

    questions = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')


class Choice(models.Model):
    poll = models.ForeignKey(Poll, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    # Create your models here.
    def __str__(self):
        return str(self.poll)
